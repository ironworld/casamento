from __future__ import with_statement
from contextlib import closing

from flask import Flask, render_template, request, session, g, redirect, url_for, abort, flash
import sqlite3

import smtplib
from email.mime.text import MIMEText


app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'secret'

# configuration
DATABASE = 'db/casamento.db'

def connect_db():
	print 'connect_db' 
	return sqlite3.connect(DATABASE)

def init_db():
	with closing(connect_db()) as db:
		with app.open_resource('schema.sql') as f:
			db.cursor().executescript(f.read())
		db.commit()

#@app.before_request
def before_request():
	print 'before_request'
	g.db = connect_db()

#@app.after_request
#def after_request(response):
def after_request():
	print 'after_request'
	g.db.close()
	#return response

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
		db = g._database = connect_to_database()
    return db

def get_db():
	"""Opens a new database connection if there is none yet for the
		current application context.
	"""
	top = _app_ctx_stack.top
	if not hasattr(top, 'sqlite_db'):
		top.sqlite_db = sqlite3.connect(app.config['DATABASE'])
		top.sqlite_db.row_factory = sqlite3.Row
	return top.sqlite_db

#@app.teardown_appcontext
#def close_connection(exception):
def close_connection():
    print 'close_connection'
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def query_db(query, args=(), one=False):
	cur = get_db().execute(query, args)
	rv = cur.fetchall()
	cur.close()
	return (rv[0] if rv else None) if one else rv
		
###### START URLs ########
@app.route('/test')
def test():
	cur = get_db().cursor()
	return render_template('teste')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/home', methods=['GET'])
def home():
    """ Home """
    return render_template('home.html')


@app.route('/fotos', methods=['GET'])
def fotos():
    """ Lista Fotos do Casamento """
    amigos = ["amigo40.jpg", "amigo41.jpg"]
    return render_template('fotos.html', amigos=amigos)


@app.route('/hoteis', methods=['GET'])
def hoteis():
    """ Lista Sugestao Hoteis """
    return render_template('hoteis.html')


@app.route('/salaodebeleza', methods=['GET'])
def salaobeleza():
    """ Lista Sugestao Salao de Beleza """
    return render_template('salaodebeleza.html')


@app.route('/grandedia', methods=['GET'])
def grandedia():
    """ Grande Dia """
    return render_template('grandedia.html')


@app.route('/singleladies', methods=['GET'])
def singleladies():
    """ Single Ladies """
    return render_template('singleladies.html')

@app.route('/cotasluademel', methods=['GET'])
def cotasluademel():
	""" Cotas de Lua de Mel """
	before_request()
	cur = g.db.execute('select id, img_name, descr, vlr, comprado, qtd, vlr_cota, msg, qtd-comprado cotas_saldo from cotas order by id')
	entries = [dict(id=row[0], img_name=row[1], descr=row[2], vlr=row[3], comprado=row[4], qtd=row[5], vlr_cota=row[6], msg=row[7], cotas_saldo=row[8]) for row in cur.fetchall()]
	after_request()
	close_connection()
	return render_template('cotasluademel.html', entries=entries)

@app.route('/cotasluademel_email/<id>/<cotas>', methods=['GET'])
def cotasluademel_email(id,cotas):
	""" Cotas de Lua de Mel - Email"""
	before_request()
	cur = g.db.execute('select id, img_name, descr, vlr, comprado, qtd, vlr_cota from cotas where id = ?', [id,])
	entries = [dict(id=row[0], img_name=row[1], descr=row[2], vlr=row[3], comprado=row[4], qtd=row[5], vlr_cota=row[6]) for row in cur.fetchall()]
	descr = entries[0]['descr']
	vlr_cota = entries[0]['vlr_cota']
	vlr_total = int(cotas) * int(vlr_cota)
	#return render_template('cotasluademel_email.html', entries=entries, cotas=cotas, descr=descr)
	after_request()
	close_connection()	
	return render_template('cotasluademel_email.html', entries=entries, cotas=cotas, descr=descr, id=id, vlr_cota=vlr_cota, vlr_total=vlr_total)

@app.route('/cotasluademel_mensagem', methods=['POST'])
def cotasluademel_mensagem():
	""" Cotas de Lua de Mel - Mensagem """
	if request.method == 'POST':
		name 		= request.form['ContactName']
		email 		= request.form['ContactEmail']
		comment 	= request.form['ContactComment']
		presente	= request.form['presente']
		cotas		= request.form['cotas']
		id			= request.form['id']
		vlr_total 	= request.form['vlr_total']

		try:
			before_request()
			# Saving Email into DB
			g.db.execute('insert into mensagem (name, email, msg) VALUES (?, ?, ?)', [name, email, comment])
			# Update Qtd Compradas
			g.db.execute('update cotas set comprado = comprado + ? where id = ?', [cotas, id])				
			g.db.commit()
			
			#### SEND EMAIL ####
			gmail_user = 'maristelaefabio2013@gmail.com'
			gmail_pwd = 'marijane'
			smtpserver = smtplib.SMTP("smtp.gmail.com",587)
			smtpserver.ehlo()
			smtpserver.starttls()
			smtpserver.ehlo
			smtpserver.login(gmail_user, gmail_pwd)

			message = 'Nome: ' + name + '\nEmail: ' + email + '\nPresente: ' + presente + '\nCotas: ' + cotas + '\nValor Total: ' + vlr_total + '\n\n' + comment
			msg = MIMEText(message,"plain", "utf-8")
			sender = 'maristelaefabio2013@gmail.com'
			recipients = ['fabiofrantzscs@yahoo.com.br', 'fabiofrantz@gmail.com', 'maristelatrojahn@gmail.com']
			msg['Subject'] = "Mensagem Casamento"		
			msg['From'] = sender
			msg['To'] = ", ".join(recipients)			
			smtpserver.sendmail(sender, recipients, msg.as_string())
			smtpserver.close()

			after_request()
			close_connection()
			
		except:
			return render_template('error.html')				

		return render_template('cotasluademel_mensagem.html', name=name, presente=presente, cotas=cotas, vlr_total=vlr_total)

"""
@app.route('/cotasluademel', methods=['GET'])
def cotasluademel():
    ''' Cotas de Lua de Mel '''
    return render_template('cotasluademel_old.html')

	@app.route('/x', methods=['GET'])
	def show_entries():
		cur = g.db.execute('select img_name, descr from cotas order by id')
		entries = [dict(img_name=row[0], descr=row[1]) for row in cur.fetchall()]
		print entries
		return render_template('cotasluademel.html', entries=entries)
"""

@app.route('/savethedate', methods=['GET'])
def savethedate():
    """ Save the Date """
    return render_template('savethedate.html')

@app.route('/elaele', methods=['GET'])
def elaele():
    """ Ela por Ele e Ele por Ela """
    return render_template('elaele.html')

###### END ########

if __name__ == '__main__':
    app.run()
