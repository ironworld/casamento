from fabric.api import *


env.git_branch = "master"
env.path = '~/sites/casamento'
env.user = 'staging'
env.hosts = ['198.211.113.35']


def pack_and_upload():
    local('git archive --format=tar %(git_branch)s | gzip > release.tar.gz' % env)
    put('release.tar.gz', '%(path)s' % env)
    with cd('%(path)s' % env):
        run("tar zxf release.tar.gz")
        run("rm release.tar.gz")
    local('rm release.tar.gz')


def restart_wsgi_process():
    with cd('%(path)s' % env):
        run("touch wsgi.py")


def install_virtual_env():
    sudo('pip install virtualenv')
    with cd('%(path)s' % env):
        run("virtualenv env")


def bootstrap():
    run('mkdir -p %(path)s' % env)
    with cd('%(path)s' % env):
        run('mkdir -p logs' % env)
    install_virtual_env()
    pack_and_upload()
    install_requirements()


def install_requirements():
    with cd('%(path)s' % env):
        run('source env/bin/activate')
        sudo('pip install -r requirements.txt')


def deploy():
    pack_and_upload()
    restart_wsgi_process()
